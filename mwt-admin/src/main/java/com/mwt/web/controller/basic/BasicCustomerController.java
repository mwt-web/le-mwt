package com.mwt.web.controller.basic;


import com.mwt.basic.domain.BasicCustomer;
import com.mwt.basic.domain.dto.BasicCustomerDTO;
import com.mwt.basic.domain.vo.BasicCustomerVO;
import com.mwt.basic.service.IBasicCustomerService;
import com.mwt.common.annotation.Log;
import com.mwt.common.annotation.RepeatSubmit;
import com.mwt.common.core.controller.BaseController;
import com.mwt.common.core.domain.AjaxResult;
import com.mwt.common.core.page.TableDataInfo;
import com.mwt.common.enums.BusinessType;
import com.mwt.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 客户档案Controller
 *
 * @author muwentao
 * @date 2022-07-20
 */
@Api("客户档案")
@RestController
@RequestMapping("/basic/customer")
public class BasicCustomerController extends BaseController {
    @Resource
    private IBasicCustomerService basicCustomerService;
   /* @Resource
    private IBasicDataService basicDataService;*/

    /**
     * 查询客户档案列表
     */
    @ApiOperation("查询客户档案列表")
    @PreAuthorize("@ss.hasPermi('basic:customer:list')")
    @GetMapping("/list")
    public TableDataInfo list(BasicCustomerDTO basicCustomer) {
        startPage();
        List<BasicCustomer> list = basicCustomerService.selectList(basicCustomer);
        return getDataTable(list);
    }

    /**
     * 获取客户档案详细信息
     */
    @ApiOperation("获取客户档案详细信息")
    @PreAuthorize("@ss.hasPermi('basic:customer:query')")
    @GetMapping(value = "/getInfo/{customerId}")
    public AjaxResult getInfo(@PathVariable("customerId") Long customerId) {
        return basicCustomerService.selectById(customerId);
    }

    /**
     * 新增客户档案
     */
    @ApiOperation("新增客户档案")
    @PreAuthorize("@ss.hasPermi('basic:customer:add')")
    @Log(title = "客户档案", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BasicCustomer basicCustomer) {
        return basicCustomerService.insert(basicCustomer);
    }

    /**
     * 修改客户档案
     */
    @ApiOperation("修改客户档案")
    @PreAuthorize("@ss.hasPermi('basic:customer:edit')")
    @Log(title = "客户档案", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BasicCustomer basicCustomer) {
        return basicCustomerService.update(basicCustomer);
    }

    /**
     * 删除客户档案
     */
    @ApiOperation("删除客户档案")
    @PreAuthorize("@ss.hasPermi('basic:customer:remove')")
    @Log(title = "客户档案", businessType = BusinessType.DELETE)
    @DeleteMapping("/{customerIds}")
    public AjaxResult remove(@PathVariable Long[] customerIds) {
        return basicCustomerService.deleteByIds(customerIds);
    }


    /**
     * 导出客户列表
     */
    @PreAuthorize("@ss.hasPermi('basic:customer:export')")
    @Log(title = "客户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, @RequestBody BasicCustomerDTO customerDTO) throws IOException {
        List<BasicCustomerVO> list = basicCustomerService.selectExportList(customerDTO);
        ExcelUtil<BasicCustomerVO> util = new ExcelUtil<>(BasicCustomerVO. class);
        util.exportExcel(response, list, "客户档案");
    }
/*
    *//**
     * 查询客户收益分享列表
     *//*
    @ApiOperation("查询所有客户信息")
    @GetMapping("/listAll")
    public AjaxResult listAll(BasicCustomerDTO basicCustomer) {
        List<BasicCustomer> list = basicDataService.getCustomerList(basicCustomer);
        return AjaxResult.success(list);
    }*/
}
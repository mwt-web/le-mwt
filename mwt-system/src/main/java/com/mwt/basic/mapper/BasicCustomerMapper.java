package com.mwt.basic.mapper;


import com.mwt.basic.domain.BasicCustomer;
import com.mwt.basic.domain.dto.BasicCustomerDTO;
import com.mwt.basic.domain.vo.BasicCustomerVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 客户档案Mapper接口
 *
 * @author muwentao
 * @date 2022-07-20
 */
public interface BasicCustomerMapper {
    /**
     * 查询客户档案
     *
     * @param customerId 客户档案ID
     * @return 客户档案
     */
        BasicCustomer selectById(Long customerId);

    /**
     * 查询客户档案列表
     *
     * @param basicCustomer 客户档案
     * @return 客户档案集合
     */
    List<BasicCustomer> selectList(BasicCustomerDTO basicCustomer);

    /**
     * 查询导出列表
     * @param customerDTO
     * @return
     */
    List<BasicCustomerVO> selectExportList(BasicCustomerDTO customerDTO);

    /**
     * 新增客户档案
     *
     * @param basicCustomer 客户档案
     * @return 结果
     */
    int insert(BasicCustomer basicCustomer);

    /**
     * 修改客户档案
     *
     * @param basicCustomer 客户档案
     * @return 结果
     */
    int update(BasicCustomer basicCustomer);

    /**
     * 删除客户档案
     *
     * @param customerId 客户档案ID
     * @return 结果
     */
    int deleteById(Long customerId);

    /**
     * 批量删除客户档案
     *
     * @param customerIds 需要删除的数据ID
     * @return 结果
     */
    int deleteByIds(Long[] customerIds);

    /**
     * 根据客户类型查询最新的一条客户编号
     * @param type
     * @return
     */
    String selectCustomerCodeByType(String type);

    /**
     * 根据客户名称查询记录
     * @param customerName
     * @return
     */
    List<Long> selectListByCustomer(String customerName);

    /**
     * 批量新增客户档案
     * @param list
     * @return
     */
    int batchInsert(@Param("list") List<BasicCustomer> list);
}
package com.mwt.basic.domain.dto;


import com.mwt.basic.domain.BasicCustomer;
import lombok.Data;

import java.util.List;

/**
 * 客户档案对象 basic_customer
 *
 * @author muwentao
 * @date 2022-07-20
 */
@Data
public class BasicCustomerDTO extends BasicCustomer
{
    /** 多个客户id,逗号拼接 */
    private String ids;

    /** 个客户id,数组 */
    private List<Long> idList;

}
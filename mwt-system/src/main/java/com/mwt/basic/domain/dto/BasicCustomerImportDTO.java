package com.mwt.basic.domain.dto;


import com.mwt.common.annotation.Excel;
import com.mwt.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 客户导入对象DTO
 *
 * @author junrunrenli
 * @date 2020-11-12
 */
@Data
public class BasicCustomerImportDTO extends BaseEntity
{

    /** 客户名称 */
    @Excel(name = "*客户名称",sort = 1)
    private String customerName;

    /** 流水合计规则 */
    @Excel(name = "*流水合计规则",sort = 2)
    private String turnoverTotalRules;

    /** 备注 */
    @Excel(name = "备注",sort = 3)
    private String remark;
}
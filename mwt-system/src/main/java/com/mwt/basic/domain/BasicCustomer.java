package com.mwt.basic.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.mwt.common.annotation.Excel;
import com.mwt.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 客户档案对象 basic_customer
 *
 * @author muwentao
 * @date 2022-07-20
 */
@Data
public class BasicCustomer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 客户id */
    private Long id;

    /** 客户类型 */
    @Excel(name = "客户类型")
    private String customerType;

    /** 客户名称 */
    @Excel(name = "客户名称")
    private String customerName;

    /** 客户编码 */
    @Excel(name = "客户编码")
    private String customerCode;

    /** 客户编码(兼容老版本加的字段) */
    private String customerNo;

    /** 状态:1-正常, 0-停用 */
    @Excel(name = "状态", readConverterExp = "1=正常, 0=停用")
    private String status;

    /** 流水合计规则:0-重庆谊品应发人数定档,1-海南融物应发人数定档,2-融创物业关联公司应发人数定档,盛宝米业开票金额定档 */
    @Excel(name = "流水合计规则", readConverterExp = "0=重庆谊品应发人数定档,1=海南融物应发人数定档,2=融创物业关联公司应发人数定档,3=盛宝米业开票金额定档")
    private String turnoverTotalRules;

    /** 禁用状态:1-是,0-否 */
    @Excel(name = "禁用状态", readConverterExp = "1=是,0=否")
    private String disabledStatus;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updatedBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    /** 是否删除 0-否 1-是 */
    @Excel(name = "是否删除 0-否 1-是")
    private String isDel;


}
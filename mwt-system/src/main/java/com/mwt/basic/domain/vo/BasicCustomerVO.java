package com.mwt.basic.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mwt.common.annotation.Excel;
import com.mwt.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 客户对象VO
 *
 * @author junrunrenli
 * @date 2020-11-12
 */
@Data
public class BasicCustomerVO extends BaseEntity
{


    /** 客户id */
    private Long id;

    /** 客户类型 */
    private String customerType;

    /** 客户编码 */
    @Excel(name = "客户编码",sort = 1)
    private String customerCode;

    /** 客户名称 */
    @Excel(name = "客户名称",sort = 2)
    private String customerName;

    /** 状态:1-正常, 0-停用 */
    private String status;

    /** 流水合计规则:0-重庆谊品应发人数定档,1-海南融物应发人数定档,2-融创物业关联公司应发人数定档,盛宝米业开票金额定档 */
    @Excel(name = "流水合计规则", readConverterExp = "0=重庆谊品应发人数定档,1=海南融物应发人数定档,2=融创物业关联公司应发人数定档,盛宝米业开票金额定档",sort = 3)
    private String turnoverTotalRules;

    /** 禁用状态:1-是,0-否 */
    @Excel(name = "禁用状态", readConverterExp = "1=是,0=否",sort = 4)
    private String disabledStatus;

    /**
     * 备注
     */
    @Excel(name = "备注",sort = 5)
    private String remark;

    /** 创建人 */
    @Excel(name = "创建人",sort = 8)
    private String createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss",sort = 9)
    private Date createdAt;

    /** 更新人 */
    @Excel(name = "更新人",sort = 6)
    private String updatedBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss",sort = 7)
    private Date updatedAt;

    /** 是否删除 0-否 1-是 */
    private String isDel;
}
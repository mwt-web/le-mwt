package com.mwt.basic.service;


import com.mwt.basic.domain.BasicCustomer;
import com.mwt.basic.domain.dto.BasicCustomerDTO;
import com.mwt.basic.domain.vo.BasicCustomerVO;
import com.mwt.common.core.domain.AjaxResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 客户档案Service接口
 *
 * @author muwentao
 * @date 2022-07-20
 */
public interface IBasicCustomerService {
    /**
     * 查询客户档案
     *
     * @param customerId 客户档案ID
     * @return 客户档案
     */
    AjaxResult selectById(Long customerId);

    /**
     * 查询客户档案列表
     *
     * @param basicCustomer 客户档案
     * @return 客户档案集合
     */
    List<BasicCustomer> selectList(BasicCustomerDTO basicCustomer);

    /**
     * 查询导出列表
     * @param customerDTO
     * @return
     */
    List<BasicCustomerVO> selectExportList(BasicCustomerDTO customerDTO);

    /**
     * 新增客户档案
     *
     * @param basicCustomer 客户档案
     * @return 结果
     */
    AjaxResult insert(BasicCustomer basicCustomer);

    /**
     * 修改客户档案
     *
     * @param basicCustomer 客户档案
     * @return 结果
     */
    AjaxResult update(BasicCustomer basicCustomer);

    /**
     * 批量删除客户档案
     *
     * @param customerIds 需要删除的客户档案ID
     * @return 结果
     */
    AjaxResult deleteByIds(Long[] customerIds);

    /**
     * 删除客户档案信息
     *
     * @param customerId 客户档案ID
     * @return 结果
     */
    int deleteById(Long customerId);

}
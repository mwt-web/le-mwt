package com.mwt.basic.service.imp;


import com.mwt.basic.domain.BasicCustomer;
import com.mwt.basic.domain.dto.BasicCustomerDTO;
import com.mwt.basic.domain.dto.BasicCustomerImportDTO;
import com.mwt.basic.domain.vo.BasicCustomerVO;
import com.mwt.basic.mapper.BasicCustomerMapper;
import com.mwt.basic.service.IBasicCustomerService;
import com.mwt.common.constant.BasicConstans;
import com.mwt.common.core.domain.AjaxResult;
import com.mwt.common.core.redis.RedisService;
import com.mwt.common.utils.SecurityUtils;
import com.mwt.common.utils.StringUtils;
import com.mwt.common.utils.poi.ExcelUtil;
import com.mwt.system.pojo.RedisKeyConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.compressors.FileNameUtil;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * 客户档案Service业务层处理
 *
 * @author muwentao
 * @date 2022-07-20
 */
@Slf4j
@Service
public class BasicCustomerServiceImpl implements IBasicCustomerService {
    @Resource
    private BasicCustomerMapper basicCustomerMapper;
    @Resource
    private RedisService redisService;

    /**
     * 查询客户档案
     *
     * @param customerId 客户档案ID
     * @return 客户档案
     */
    @Override
    public AjaxResult selectById(Long customerId) {
        return AjaxResult.success(basicCustomerMapper.selectById(customerId));
    }

    /**
     * 查询客户档案列表
     *
     * @param basicCustomer 客户档案
     * @return 客户档案
     */
    @Override
    public List<BasicCustomer> selectList(BasicCustomerDTO basicCustomer) {
        return basicCustomerMapper.selectList(basicCustomer);
    }

    /**
     * 新增客户档案
     *
     * @param basicCustomer 客户档案
     * @return 结果
     */
    @Override
    public AjaxResult insert(BasicCustomer basicCustomer) {
        if (ObjectUtils.isEmpty(basicCustomer.getCustomerName())) {
            return AjaxResult.error("客户名称不能为空");
        }
        List<Long> list = basicCustomerMapper.selectListByCustomer(basicCustomer.getCustomerName());
        if (ObjectUtils.isNotEmpty(list)) {
            return AjaxResult.error("客户名称重复");
        }
        String key = "BATCH_NO:" + RedisKeyConstant.DATA_CUSTOMER_CODE_KEY;
        Integer code = redisService.getCacheObject(key);
        if (ObjectUtils.isEmpty(code)) {
            String customerCode = basicCustomerMapper.selectCustomerCodeByType("2");
            String rx = StringUtils.remove(customerCode, "RX");
            code = Integer.parseInt(rx);
        }
        String customerCode = redisService.getIncreStr(RedisKeyConstant.DATA_CUSTOMER_CODE_KEY, BasicConstans.BATCH_NO_PREFIX_CUSTOMER_CODE, 7, "", code);
        basicCustomer.setCustomerCode(customerCode);
        basicCustomer.setCustomerType(BasicConstans.customerType.INTERNAL_SYSTEM);
        basicCustomer.setCreatedBy(SecurityUtils.getUsername());
        basicCustomer.setUpdatedBy(SecurityUtils.getUsername());
        basicCustomerMapper.insert(basicCustomer);
        String redisKey = RedisKeyConstant.DATA_CUSTOMER_INFO_KEY + "*";
        Collection<String> keysByBasic = redisService.keys(redisKey);
        redisService.deleteObject(keysByBasic);
        return AjaxResult.success();
    }

    /**
     * 修改客户档案
     *
     * @param basicCustomer 客户档案
     * @return 结果
     */
    @Override
    public AjaxResult update(BasicCustomer basicCustomer) {
        if (ObjectUtils.isEmpty(basicCustomer.getCustomerName())) {
            return AjaxResult.error("客户名称不能为空");
        }
        List<Long> list = basicCustomerMapper.selectListByCustomer(basicCustomer.getCustomerName());
        if (ObjectUtils.isNotEmpty(list)) {
            for (long id :
                    list) {
                if (!basicCustomer.getId().equals(id)) {
                    return AjaxResult.error("客户名称重复");
                }
            }
        }
        basicCustomer.setUpdatedBy(SecurityUtils.getUsername());
        basicCustomer.setUpdatedAt(new Date());
        basicCustomerMapper.update(basicCustomer);
        String redisKey = RedisKeyConstant.DATA_CUSTOMER_INFO_KEY + "*";
        Collection<String> keysByBasic = redisService.keys(redisKey);
        redisService.deleteObject(keysByBasic);
        return AjaxResult.success();
    }

    /**
     * 批量删除客户档案
     *
     * @param customerIds 需要删除的客户档案ID
     * @return 结果
     */
    @Override
    public AjaxResult deleteByIds(Long[] customerIds) {
        basicCustomerMapper.deleteByIds(customerIds);
        String redisKey = RedisKeyConstant.DATA_CUSTOMER_INFO_KEY + "*";
        Collection<String> keysByBasic = redisService.keys(redisKey);
        redisService.deleteObject(keysByBasic);
        return AjaxResult.success();
    }

    /**
     * 删除客户档案信息
     *
     * @param customerId 客户档案ID
     * @return 结果
     */
    @Override
    public int deleteById(Long customerId) {
        int i = basicCustomerMapper.deleteById(customerId);
        String redisKey = RedisKeyConstant.DATA_CUSTOMER_INFO_KEY + "*";
        Collection<String> keysByBasic = redisService.keys(redisKey);
        redisService.deleteObject(keysByBasic);
        return i;
    }

    @Override
    public List<BasicCustomerVO> selectExportList(BasicCustomerDTO customerDTO) {
        return basicCustomerMapper.selectExportList(customerDTO);
    }
}

package com.mwt.system.pojo;

/**
 * 收入明细表相关常量
 *
 * @author White
 * @date 2021/3/16
 */
public class IncomeConstant {

    /**
     * 客户
     */
    public static final String CUSTOMER_ERROR_NULL = "第%s行:客户牌照数据项为空，请核实后重新上传";
    public static final String CUSTOMER_ERROR_NOT_EXIST = "第%s行:{%s}客户不存在或已失效，请核实后重新上传";

    /**
     * 大区
     */
    public static final String AREA_ERROR_NULL = "第%s行:归属大区数据项为空，请核实后重新上传";
    public static final String AREA_ERROR_NOT_EXIST = "第%s行:{%s}大区不存在或已失效，请核实后重新上传";

    /**
     * 产品
     */
    public static final String PRODUCT_ERROR_NULL = "第%s行:产品名称数据项为空，请核实后重新上传";
    public static final String PRODUCT_ERROR_NOT_EXIST = "第%s行:{%s}产品不存在或已失效，请核实后重新上传";

    /**
     * 销售方全称
     */
    public static final String LICENSE_ERROR_NULL = "第%s行:销售方全称数据项为空，请核实后重新上传";
    public static final String LICENSE_ERROR_NOT_EXIST = "第%s行:{%s}牌照不存在或已失效，请核实后重新上传";

    /**
     * 应收金额
     */
    public static final String YT_RECEIVABLE_AMOUNT_NULL = "第%s行:应收金额(含税)为空";

    /**
     * 管理服务费（含税）
     */
    public static final String YT_VALUE_ADDED_SURCHARGE_AMOUNT = "第%s行:管理服务费（含税）为空";

    /**
     * 增值税税率
     */
    public static final String VALUE_ADDED_TAX_RATE = "第%s行:增值税税率为空";

    /**
     * 增值税附加税税率
     */
    public static final String ADDITIONAL_TAX_RATE = "第%s行:增值税附加税税率为空";

    /**
     * 其他补扣
     */
    public static final String OTHER_DEDUCTION_AMOUNT = "第%s行:其他补扣为空";

    /**
     * 其他
     */
    public static final String OTHER_AMOUNT = "第%s行:其他为空";

    /**
     * 合同返税信息
     */
    public static final String BELONG_CONTRACT_OR_REBATE_NULL = "第%s行:没有对应的合同返税信息";


}

package com.mwt.system.pojo;


/**
 * MQ常量
 *
 * @author White
 * @date 2021/3/17
 */
public class MQConstant {

    public final static String ROCKET_TAGS = "rocketmq_TAGS";

    public final static String INSERT_TAG = "create";

    public final static String UPDATE_TAG = "update";

    public final static String CONTRACT_UPDATE_TAG = "updateContract";
    public final static String CONTRACT_CREATE_TAG = "createContract";


}

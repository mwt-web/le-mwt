package com.mwt.system.pojo;

import lombok.Data;

/**
 * Excel 行处理信息
 *
 * @author White
 * @date 2021/3/16
 */
@Data
public class ExcelRowDealInfo {

    private Boolean result;

    private String type;

    private String msg;

}

package com.mwt.system.pojo;

/**
 * redis key 常量
 *
 * @author White
 * @date 2021/3/17
 */
public class RedisKeyConstant {

    /**
     * 业务大区、客户牌照、销售方(我方)牌照、产品
     */
    public final static String DATA_AREA_KEY = "dataArea";
    public final static String DATA_CUSTOMER_KEY = "dataCustomer";
    public final static String DATA_LICENSE_KEY = "dataLicense";
    public final static String DATA_PRODUCT_KEY = "dataProduct";

    /**
     * 渠道商
     */
    public final static String DATA_CHANNEL_INFO_KEY = "basic:channelInfo";
    /**
     * 客户
     */
    public final static String DATA_CUSTOMER_INFO_KEY = "basic:customer";
    /**
     * 业务大区
     */
    public final static String DATA_REGION_AREA_KEY = "basic:regionArea";

    /**
     * 销售方
     */
    public final static String DATA_LICENSE_INFO_KEY = "basic:license";

    /**
     * 产品
     */
    public final static String DATA_PRODUCT_INFO_KEY = "basic:product";

    /**
     * 渠道商返还规则
     */
    public final static String DATA_CHANNEL_RULES_KEY = "basic:channelRules";
    /**
     * 渠道商返还时间规则
     */
    public final static String DATA_CHANNEL_TIME_RULES_KEY = "basic:channelTimeRules";

    /**
     * 合同外规则
     */
    public final static String DATA_CONTRACT_EXTERNAL_RULES_KEY = "basic:contractExternalRules";
    /**
     * 商品税目信息
     */
    public final static String DATA_TAX_INFO_KEY = "basic:taxInfo";

    /**
     *  流程-归属区/部门
     */
    public final static String DATA_PROCESS_DEPARTMENT_KEY = "basic:process:oaDepartment";

    /**
     *  流程-归属区/部门
     */
    public final static String DATA_PROCESS_INVOICE_TYPE_KEY = "basic:process:oaInvoiceType";

    /**
     *  流程-归属区/部门
     */
    public final static String DATA_PROCESS_TAX_RATE_KEY = "basic:process:oaTaxRate";

    /**
     *  流程-归属区/部门
     */
    public final static String DATA_PROCESS_ORG_KEY = "basic:process:org";

    /**
     * 收入明细基础key
     */
    public final static String INCOME_BASIC_KEY = "incomeBasic";

    /**
     * 合同、合同税反信息
     */
    public final static String CONTRACT_KEY = "contract";
    public final static String CONTRACT_REBATE_KEY = "contractRebate";

    /**
     * 渠道商合同
     */
    public final static String CONTRACT_CHANNEL_KEY = "contractChannel";
    public final static String CONTRACT_CHANNEL_IMPORT_KEY = "contractChannelImport";

    /**
     * 客户合同
     */
    public final static String CONTRACT_CUSTOMER_KEY = "contractCustomer";
    public final static String CONTRACT_CUSTOMER_IMPORT_KEY = "contractCustomerImport";


    /**
     * 渠道商管理
     */
    public final static String DATA_CHANNEL_CODE_KEY = "channel";
    public final static String CHANNEL_IMPORT_KEY = "channel:Import";

    /**
     * 客户管理
     */
    public final static String DATA_CUSTOMER_CODE_KEY = "customer";
    public final static String CUSTOMER_IMPORT_KEY = "customer:Import";



    /**
     * 返还时间规则映射
     */
    public final static String DATA_CHANNEL_TIME_RULES_CAST = "basic:channelTimeRulesCast";

    /**
     * 渠道商收入数据导入
     */
    public final static String DATA_CHANNEL_INCOME_DATA_IMPORT_KEY = "business:channelIncomeData";

    /**
     * 渠道商收益分享编号
     */
    public final static String DATA_CHANNEL_REVENUE_SHARE_CODE_KEY = "business:channelRevenueShareCode";
    /**
     * 客户收益分享编号
     */
    public final static String DATA_CUSTOMER_REVENUE_SHARE_CODE_KEY = "business:customerRevenueShareCode";

    /**
     * 客户收益分享任务编号
     */
    public final static String DATA_CUSTOMER_REVENUE_SHARE_TASK_CODE_KEY = "business:customerTask";


}

/*
package com.mwt.common.utils.thread;

import com.mwt.common.core.domain.entity.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

*/
/**
 * @author liangyongpeng
 * @create 2022/1/7 9:44
 *//*

@Slf4j
public class RunnableImpCrmOutsideUtil implements Runnable {
 
    private final SystemUtils outsideMapper;
 
    private List<SysUser> results = new ArrayList<>();
 
    */
/**
     * java 1.5 java.util.cucurrent包下 主要作用是 一个线程 等待其他线程执行完之后，在开始执行
     * 是通过一个计数器来实现的，计数器的初始值是线程的数量。每当一个线程执行完毕后，计数器的值 
     * 就-1，当计数器的值为0时，表示所有线程都执行完毕，然后在闭锁上等待的线程就可以恢复工作了。
     *//*

    private final CountDownLatch latch;
 
    private String type = "";
 
    public RunnableImpCrmOutsideUtil(CrmCustOutsideMapper outsideMapper, List<SysUser> results, CountDownLatch latch, String type){
        if (StringUtils.isEmpty(type)) {
            throw new RuntimeException("类型不能为空");
        }
        this.outsideMapper = outsideMapper;
        this.results = results;
        this.latch = latch;
        this.type = type;
    }
 
    @Override
    public void run() {
        if (CollectionUtils.isNotEmpty(results)) {
            // CK
            if ("0".equals(type)) {
                outsideMapper.insertCrmCustOutside(results);
            }
            // Mysql
            if ("1".equals(type)) {
                outsideMapper.insertCrmCustOutsideToMysql(results);
            }
        }
        // 线程执行完 触发计数器减一
        latch.countDown();
    }
 
}*/

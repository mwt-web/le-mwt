package com.mwt.common.constant;

/**
 * 主数据常量
 * @author muwentao
 */
public class BasicConstans {

    /**
     *
     * 渠道商管理-渠道商编码
     */
    public static final String BATCH_NO_PREFIX_CHANNEL_CODE = "QD";

    /**
     *
     * 渠道商合同
     */
    public static final String BATCH_NO_PREFIX_CHANNEL_CONTRACT_CODE = "RXHT";

    /**
     *
     * 渠道商收益分享编码
     */
    public static final String BATCH_NO_PREFIX_CHANNEL_REVENUE_SHARE_CODE = "QDSY";

    /**
     *
     * 渠道商管理-渠道商编码
     */
    public static final String BATCH_NO_PREFIX_CUSTOMER_CODE = "RX";

    /**
     *
     * 客户合同
     */
    public static final String BATCH_NO_PREFIX_CUSTOMER_CONTRACT_CODE = "KHHT";
    /**
     *
     * 客户收益分享编码
     */
    public static final String BATCH_NO_PREFIX_CUSTOMER_REVENUE_SHARE_CODE = "KHSY";

    /**
     *
     * 客户收益分享任务编码
     */
    public static final String BATCH_NO_PREFIX_CUSTOMER_REVENUE_SHARE_TASK_CODE = "R";

    /**
     * 1-是
     */
    public static final String YES="1";

    /**
     * 0-否
     */
    public static final String NO="0";

    /**
     * 计算基准
     */
    public static class calculateBenchmark {
        /**
         * 1-开票金额
         */
        public static final String INVOICE_AMOUNT = "1";

        /**
         * 2-实发工资
         */
        public static final String REAL_SALARY = "2";
        /**
         * 3-应发工资
         */
        public static final String SALARY = "3";
        /**
         * 4-管理服务费（含税）
         */
        public static final String SERVICE = "4";
        /**
         * 5-应收金额（含税）
         */
        public static final String RECEIVER_AMOUNT = "5";
        /**
         * 6-无
         */
        public static final String NIX = "6";

        /**
         * 7-开票金额(全海项目)
         */
        public static final String INVOICE_AMOUNT_QH = "7";

        /**
         * 8-实发工资(驿加易项目)
         */
        public static final String REAL_SALARY_YJY = "8";
    }

    /**
     * 时间计算基准
     */
    public static class timeCalculateBenchmark {
        /**
         * 1-结算月份
         */
        public static final String MONTH = "1";

        /**
         * 2-结算季度
         */
        public static final String QUARTER = "2";
        /**
         * 3-实际月份
         */
        public static final String REAL_MONTH = "3";
        /**
         * 4-无
         */
        public static final String NIX = "4";
    }

    /**
     * 返还类别
     */
    public static class returnType {
        /**
         * 1-增值税返还
         */
        public static final String VAT = "1";

        /**
         * 2-服务费返还
         */
        public static final String SERVICE = "2";
    }

    /**
     * 返还方式  1=固定比例,2=阶梯式,3=无
     */
    public static class returnWay {
        /**
         * 1-固定比例
         */
        public static final String FIXED_SCALE = "1";

        /**
         * 2-阶梯式
         */
        public static final String STEP_WISE = "2";

        /**
         * 3-无
         */
        public static final String NIX = "3";
    }
    /**
     * 收益分享方式:1-不含附加及管理,2-不含附加,3-不含管理
     */
    public static class taxReturnWay {
        /**
         * 1-不含附加及管理
         */
        public static final String NOT_ADDITIONAL_MANAGE = "1";

        /**
         * 2-不含附加
         */
        public static final String NOT_ADDITIONAL = "2";

        /**
         * 3-不含管理
         */
        public static final String NOT_MANAGE = "3";
    }

    /**
     * 客户类型:1-数据中台,2-内部系统
     */
    public static class customerType {
        /**
         * 1-数据中台
         */
        public static final String DATA_CENTER = "1";

        /**
         * 2-内部系统
         */
        public static final String INTERNAL_SYSTEM = "2";
    }

    /**
     * 流水合计规则:0-重庆谊品应发人数定档,1-海南融物应发人数定档,2-融创物业关联公司应发人数定档,3-盛宝米业开票金额定档
     */
    public static class  turnoverTotalRules{
        /**
         * 0-重庆谊品应发人数定档
         */
        public static final String CQYP = "0";

        /**
         * 1-海南融物应发人数定档
         */
        public static final String HNRW = "1";
        /**
         * 2-融创物业关联公司应发人数定档
         */
        public static final String RCWY = "2";
        /**
         *3-盛宝米业开票金额定档
         */
        public static final String SBMY = "3";
    }

}

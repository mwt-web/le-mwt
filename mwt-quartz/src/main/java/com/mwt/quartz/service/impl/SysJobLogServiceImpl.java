package com.mwt.quartz.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mwt.quartz.domain.SysJobLog;
import com.mwt.quartz.mapper.SysJobLogMapper;
import com.mwt.quartz.service.ISysJobLogService;

import javax.annotation.Resource;

/**
 * 定时任务调度日志信息 服务层
 * 
 * @author mwt
 */
@Slf4j
@Service
public class SysJobLogServiceImpl implements ISysJobLogService
{
    @Resource
    private SysJobLogMapper jobLogMapper;

    /**
     * 获取quartz调度器日志的计划任务
     * 
     * @param jobLog 调度日志信息
     * @return 调度任务日志集合
     */
    @Override
    public List<SysJobLog> selectJobLogList(SysJobLog jobLog)
    {
        List<SysJobLog> list=new ArrayList<>();
        for (int i = 0; i<10000000; i++) {
            SysJobLog job=new SysJobLog();
            job.setJobGroup("test");
            job.setJobName("穆文涛");
            job.setJobMessage("插入数据信息");
            job.setInvokeTarget("不知道干啥的");
            job.setStartTime(new Date());

            list.add(job);
            //jobLogMapper.insertJobLog(job);
        }

        long start = System.currentTimeMillis();
        int index=0;
        int number=0;
        int intervalNumber=3000;
        int insertRow = list.size();
        while (insertRow>intervalNumber){
            List<SysJobLog> subList = list.subList(index, index+intervalNumber);
            jobLogMapper.batchInsertJobLog(subList);
            index+=intervalNumber;
            insertRow-=intervalNumber;
            number++;
            log.info("执行次数：{}",number);
        }
        if(list.size()>0){

            jobLogMapper.batchInsertJobLog(list);
            log.info("最后一次执行新增，共耗时:{}毫秒",System.currentTimeMillis()-start);
        }
        return jobLogMapper.selectJobLogList(jobLog);
    }

    /**
     * 通过调度任务日志ID查询调度信息
     * 
     * @param jobLogId 调度任务日志ID
     * @return 调度任务日志对象信息
     */
    @Override
    public SysJobLog selectJobLogById(Long jobLogId)
    {
        return jobLogMapper.selectJobLogById(jobLogId);
    }

    /**
     * 新增任务日志
     * 
     * @param jobLog 调度日志信息
     */
    @Override
    public void addJobLog(SysJobLog jobLog)
    {
        jobLogMapper.insertJobLog(jobLog);
    }

    /**
     * 批量删除调度日志信息
     * 
     * @param logIds 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteJobLogByIds(Long[] logIds)
    {
        return jobLogMapper.deleteJobLogByIds(logIds);
    }

    /**
     * 删除任务日志
     * 
     * @param jobId 调度日志ID
     */
    @Override
    public int deleteJobLogById(Long jobId)
    {
        return jobLogMapper.deleteJobLogById(jobId);
    }

    /**
     * 清空任务日志
     */
    @Override
    public void cleanJobLog()
    {
        jobLogMapper.cleanJobLog();
    }
}
